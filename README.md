# Mastering Git course

![xkcd Git](https://explainxkcd.com/wiki/images/4/4d/git.png)

6 weeks course on mastering git.

## About the course
Git is a powerful and easy to learn software which is mainly used for
collaborative software development. During this course you will learn unique
benefits and concepts of [Git](https://git-scm.com/) as well as its core features via extensive
examples and hands-on exercises. We hope you will develop the joy of working
with it. This course doesn’t require any prior knowledge of Git and is perfect
for anyone who wants a jump start in learning it. If you already have some
experience with Git, then this course will make you an expert, since it gives a
deep look at the fundamental commands.

## Objective
During this course you will learn unique benefits and concepts of Git as well
as its core features via extensive examples, hands-on exercises and mandatory
homework. The emphasis of the course is to prepare you for working on group
community upstream projects in enterprise. We want you to experience what it's
like to work on a Git project where multiple people contribute: this is what we
experience in our jobs every day.

## Lectors
* Irina Gulina (Senior Software Quality Engineer)
* [Tomáš Tomeček](https://github.com/TomasTomecek) (Senior Principal Software Engineer)

You can learn more about this course in an interview with the lectors: [Red Hat
Research: Mastering Git with university
students](https://research.redhat.com/blog/2023/02/28/mastering-git-with-university-students/)

And another interview with students:
[Intern Spotlight: Red Hat course helps students unleash the power of Git](https://research.redhat.com/blog/2023/09/11/intern-spotlight-red-hat-course-helps-students-unleash-the-power-of-git/)

## Current run
We are teaching this course now in autumn 2023 at FI MUNI Brno as part of PV177
Laboratory of Advanced Network Technologies.

The course is open to all students: Bc, Mgr and PhD.

**All groups are now full.**

This is the second run. The first one happened in Autumn 2022.

**IS course info**: https://is.muni.cz/predmet/fi/podzim2023/PV177  
**Lecture time**: Group 1 10:00-11:40, Group 2 12:00-13:40
**Lecture room**: FI MU S505 Red Hat lab at Faculty of Informatics, Masaryk University (S505, note that the room is only available during lecture time)  
**Grading**: successfully complete 5 mandatory homeworks, or 4/5 homeworks plus a bonus task  
**ECTS Credits**: 2  

## Prerequisites
* Be comfortable in a command-line environment.
* Understand basic programming concepts.
* Understand basic Computer Science concepts.
* Linux-based operating system is a big plus. You may get bonus points for Fedora Linux 😇

## Homeworks

### Requirements after first class

* Fork this repository
* Upload SSH keys to your account
* Comment your UCO in an issue in this repo for your respective group:
  0. https://gitlab.com/redhat/research/mastering-git/-/issues/2
  1. https://gitlab.com/redhat/research/mastering-git/-/issues/3

### Class 2 homework

**Deadline**: 11. October 2023, 23:59

* Create a new file in `class2_homework/` subdirectory of this repository with
  a name `<UCO>.txt`. Content of the file is up to you: it can be your
  favourite hobby, a cooking recipe, a book, ...
* Commit the change.
* Push the change to your fork, **not** to the main branch (Hint: imagine a
  merge request would be a next step).
* After you've done all of the above, save your command history, especially all
  `git` binary invocations. Put the history in a new file `<UCO>_commands.txt`
  and place it in the same directory and the same branch, commit it and push
  it. We want to see how you use git so we're sure we're teaching effectively.
  This is how to directory should look:
```bash
$ tree class2_homework/
 class2_homework
 ├── 255490_commands.txt
 └── 255490.txt

 1 directory, 2 files

$ cat class2_homework/*
git foo
git bar
cat dog

My favourite book series now is from Jeffery Deaver: Lincoln Rhyme.
```


### Class 3 homework

**Deadline**: 18. October 2023, 23:59

The outcome of class 3 homework will be a merge request in this repository.
It's similar as class 2 homework, except we want a merge request this time and
it needs to be merged.

- Create a new file in `class3_homework/` subdirectory of this repository with
  a name `<UCO>.txt`. Tell us what you enjoy so far on the course, what new you
  have learned and used already. We'd also appreciate if something is missing.
- Make sure the merge request complies with all the best practices from the class.
- You have to create the merge request in this upstream repository. Merge
  requests in forks (origin remote) will be ignored.
- Tomas and Irina will review all upstream merge requests: you may be asked to
  correct something.
- Your merge request needs to be merged to pass this class.
- After you've done all of the above, save your command history, especially all
  `git` binary invocations. Put the history in a new file `<UCO>_commands.txt`
  and place it in the same directory and the same branch, commit it and push
  it. We want to see how you use git so we're sure we're teaching effectively.
  If you are asked to change something, no need to account for these commands.


### Class 4 homework

**Deadline**: 25. October 2023, 23:59

Resolving merge conflicts is fun, right? Let's do some of that.

We have an old outdated branch in this upstream repository that someone worked
on long time ago. There are some valuable changes in there, while some content
is likely outdated.

**Task**: rebase `upstream/class4-hw-problem` against `upstream/main-oct-9`.
Create a merge request against `upstream/main-oct-9` from your fork from branch
`class4-hw-problem` with your proposal. You'll pass this HW once your MR is
approved.

- There will be merge conflicts in README.md and it's up to you to resolve
  them. Use your best judgement to pick changes you like and discard those that
  are outdated. There is no best solution here, we will review all MRs individually.
- Make sure there are **NO** merge conflict artifacts in your MR (such as `+<<<<<<< HEAD`).
- You have to create the merge request in this upstream repository. Merge
  requests in forks (origin remote) will be ignored.
- Make sure the merge request complies with all the best practices from the class.
- Hint: commit message may need some updating. (no need to worry about commit
  authorship)
- After you're done with the rebase process, save your command history,
  especially all `git` binary invocations. Put the history in a new file
  `<UCO>_commands.txt` inside `class4_homework/` directory.  Commit and push
  the file into the MR. If you are asked to change something, no need to
  account for these commands in the history file.
- Tomas and Irina will review all upstream merge requests: you may be asked to
  correct something.

### Class 5 homework

**Deadline**: 8. November 2023, 23:59

- Pair with any student from your class. If you did a similar lab in a class, please switch roles and be Student #1 or #2 accordingly. 
- Student #1 shares any git repo (any “Example”, but not Mastering git) with Student #2 and Irina/Tomas.
- Student #2 makes a contribution to an existing file inside Student's #1 git repo by opening a MR.
- Student #1 will push a change to the repo ("Example") that will cause a merge conflict in Student's #2 MR
- Student #2 resolves the merge conflict.
- After conflict is resolved, Student #1 asks to change something else in the MR. Based on a requested change, Student #2 needs to do a rebase again or add an additional commit. 
- MR should be merged in the end and or rejected/closed if no positive value.
- Afterwards, Student #1 makes any contribution to their own (“Example”) repo via MR, and Student #2 needs to process it accordingly.
- Both students shall mind/practise/demonstrate best Git practices on commits and MR.
- All MRs during this HW should also contain a file (in a separate commit) with a shell history. In a case of rebase / new MR change, a new history file should be attached accordingly. So, in the end, each MR may contain several commits demonstrating git history. 
- In a case of any troubles tag us on a MR to help, or just share what difficulties you had in MR conversations, so we can address the most common troubles on the last git class.


### Bonus Task

Make a contribution to an Open Source project of your choice which doesn't have any relation to MUNI and not started/owned by you. Another requirement - it should be meaningful, introduce a positive change. Contributions like "This project is to practice your git skills. Add your name to this file and we will merge it." won't be counted. To find projects to contribute for example please look at: 

* [First Timers Only](https://www.firsttimersonly.com/)
* [10 C++ open source projects welcoming contributions](https://blog.codacy.com/10-cpp-open-source-projects/)
* [Contributions-welcome topics on GitHub](https://github.com/topics/contributions-welcome)
* [Hacktoberfest - 10th anniversary](https://hacktoberfest.com/). Check on [participation info](https://hacktoberfest.com/participation/)

It's ok if a PR/MR is open, but not merged yet by the deadline time. Add a `<UCO>.txt` file into `bonus_task` directory which contains a link to you contribution and maybe some words about it - anything you want to share about it. 

## Sylabus
(This will be polished during summer 2023 and will include materials)

* Introduction of this course, organization, motivation, intro to VCS and git
  * How did developers manage code before version control and why was it bad?
  * Definition of version control and what can you do with it.
  * Introduction to git.
  * Installing Git on your workstation (focus on Linux, other OS best effort)
  * The basics of Git Workflow and how to start with git
  * The most essential functionality: Cloning Repositories.
  * Basics of git: index, working tree, local repository, remote repository, configuration of git repositories, securing repositories with SSH keys.
* How does branching work in git
  * Art of commits.
  * Best practices for branching.
  * Git tags: why types there, how to use them and what’s their use case?
  * Stash: put your current work on a shelf and restore it later.
  * Moving and merging code: what to do when someone wants to contribute, how to accept it.
* Fixing mistakes
  * How to revert a change?
  * Look into history what has changed, audit changes done in a repository.
  * Who has changed this file and why and when?
  * Resolving conflicts between changes.
  * The holy grail of git: interactive rebase. You *need to* understand this.
  * Different ways to find and undo changes made to a Git project and when to use them.
* Working as a team with a git repository
  * Deep dive into remotes. What’s upstream and a fork?
  * How to work with multiple remote repositories?
  * Moving changes between remote repositories: push, pull and fetch
  * How can I check out someone else’s changes locally?
  * The golden rule of push.
  * Tracking remote repositories.
* Git Etiquette
  * How to write a good commit message?
  * Best practices for contributing to open source projects.
  * How to submit a pull request?
  * How to review a pull request?
  * You need to know how to use force-push properly.
* Git features and common open source git workflows
  * How to create an issue in an open source project?
  * Git interfaces and integrations
  * More content based on students' feedback
